/**
 * @file
 * Builds Annotation Chart.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.annotationchart_gva = {
    attach: function (context) {
      $.each($('.annotationchart-chart-gva', context).not('.annotationchart-processed'), function (idx, value) {
        google.setOnLoadCallback(function () {
          drawChart(value);
        });
      });
    },
    detach: function (context) {
    }
  };

  function drawChart(value) {
    var chart_id = $(value).attr('id');
    var chart = drupalSettings.annotationchart[chart_id];
    $(value).addClass('annotationchart-processed');
    var data = new google.visualization.DataTable();
    data.addColumn('date', chart.options['labels'][0][0]);
    data.addColumn('number', chart.options['labels'][0][1]);
    data.addColumn('string', chart.options['labels'][0][2]);
    if (chart.options['graphfield2'] === 1) {
      data.addColumn('number', chart.options['labels'][0][3]);
    }
    chart.dataArray.shift();
    var key;
    for (key in chart.dataArray) {
      if (chart.dataArray.hasOwnProperty(key)) {
        if (chart.options['graphfield2'] === 1) {
          data.addRows([
            [new Date(chart.dataArray[key][0]),
              chart.dataArray[key][1],
              chart.dataArray[key][2],
              chart.dataArray[key][3]]
          ]);
        }
        if (chart.options['graphfield2'] === 0) {
          data.addRows([
            [new Date(chart.dataArray[key][0]),
              chart.dataArray[key][1],
              chart.dataArray[key][2]]
          ]);
        }
      }
    }
    chart.options['width'] = '100%';
    chart.options['height'] = '100%';
    chart.options['fill'] = 75;
    chart.options['allowHtml'] = true;
    chart.options['scaleColumns'] = [1, 3];
    chart.options['colors'] = [chart.options.color, chart.options.color2];
    var chartElement = document.getElementById(chart.chart_id);
    Drupal.annotationchart.charts[chart_id] = new google.visualization.AnnotationChart(chartElement);
    if (typeof Drupal.annotationchart.charts[chart_id] !== 'undefined') {
      Drupal.annotationchart.charts[chart_id].resize = function (width, height) {
        if (typeof width !== 'undefined') {
          chart.options['width'] = width;
        }
        if (typeof height !== 'undefined') {
          chart.options['height'] = height;
        }
        this.draw(data, chart.options);
      };
    }
    Drupal.annotationchart.charts[chart_id].draw(data, chart.options);

  }

})(jQuery, Drupal, drupalSettings);
