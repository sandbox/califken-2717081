/**
 * @file
 */

(function () {

  'use strict';

  google.load('visualization', '1', {packages: ['corechart', 'annotationchart']});

  Drupal.annotationchart = Drupal.annotationchart || {};
  Drupal.annotationchart.charts = Drupal.annotationchart.charts || {};

})(jQuery);
