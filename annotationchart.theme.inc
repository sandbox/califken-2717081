<?php
/**
 * @file
 * Theme file.
 */

use Drupal\Core\Template\Attribute;

/**
 * Preprocess the Annotation Chart data.
 *
 * Used by both regular calls to theme() and the annotationchart Views handler.
 */
function template_preprocess_annotationchart(&$vars) {
  if (!empty($vars['view'])) {
    $view = $vars['view'];
    $options = $view->style_plugin->options;

    // Fields.
    foreach ($view->field as $name => $field) {
      $options['fields'][$name]['label'] = $field->options['label'];
    }

    // Title.
    if (empty($options['title'])) {
      $options['title'] = $view->getTitle();
    }

    // Data.
    $data = $view->style_plugin->getRenderFields();
  }
  else {
    $options = $vars['options'];
    $data = $options['data'];
  }

  $chart_id = 'annotationchart_' . uniqid();
  $plugin = annotationchart_plugin($options['type']);
  $vars['chart'] = $plugin->render($chart_id, $data, $options);

  $vars['chart_attributes'] = new Attribute(
    array(
      'id' => $chart_id,
      'class' => array(
        'annotationchart-chart',
        'annotationchart-chart-' . $plugin->name,
      ),
    )
  );

  $plugin->postRender();
}
