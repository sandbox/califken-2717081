CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Dependencies
 * Setting up a View
 * Instructions
 * Roadmap
 * Known Problems
 * Credits
 * Similar Projects and how are they different?
 * Dependencies


INTRODUCTION
------------

 * Annotation charts are interactive time series line charts that support
   annotations.


DEPENDENCIES
------------

 * Google API Loader - https://developers.google.com/loader/


SETTING UP A VIEW
-----------------

 * This is a View Style plugin that requires 3 field types:

   - Date Field date: in HTML Time / Date format (2016-05-16T12:00:00-0700)

   - Title Field string: charted as the annotated event titles

   - Numeric Field integer: charted as the values on the line graph


INSTRUCTIONS
------------

 * Create a View

   - Add a Page or Block Display

   - Select Table format

 * Add a Date field

   - For example, Authored by

   - Create a label

   - Select the HTML Datetime format

 * Add an Event Title field

   - For example, node Title

   - Create a label

 * Add a Graph Value field

   - For example, Comment Count

   - Create a label

 * Setup and Save

   - Setup any other filters, contexts, relationships, etc.. as required to
     build your view

   - Verify the results in the View preview

   - Save the View

 * Duplicate the Page as an Attachment

   - Attach the duplicate to the original Page

   - Next to Format, click Table to change the display style

   - Select For This page (override)

   - Select Annotation Chart

   - Click Apply

 * Next to Format: Annotation Chart, click Settings

   - For Timeline Date Values, designate the Date field

   - For Graph Values, designate 1 - 2 numeric Graph value fields

   - For Event Values, designate the Event Title field

   - Optionally customize the Graph Colors

   - Click Apply

 * Save the View

 * Go to the URL for the Page View and infer some new insights!


 ROADMAP
 -------

  * Incorporate features and a UI to facilitate building a chart that includes a
    broader variance of data sources

    - Build a chart to display on content pages that shows all revisions as
      annotations, with the graph representing the daily content views. From
      this chart, content authors may infer how revisions to their content have
      affected their content's popularity.

    - Build a chart that integrates Google's Page Speed Insights API, with
      Drupal administrative and system events as annotations. From this chart,
      administrators may infer how enabling certain modules has affected page
      load and render times over time.

    - Build a chart that incorporates the results from multiple views.

 * Integrate additional chart options

   - Allow HTML in annotation descriptions

   - Values Suffix

   - Annotations Width

   - Display Annotations Filter

   - Display Legend Values

   - Display Range Selector

   - Display Zoom Buttons

   - Graph Fill Opacity

   - Graph Line Thickness

   - Legend Position


KNOWN PROBLEMS
--------------

 * Views UI Preview doesn't display an Annotated Chart.


CREDITS
-------

Current maintainers:
 * Kenneth Caple (califken) - https://www.drupal.org/u/califken


SIMILAR PROJECTS AND HOW ARE THEY DIFFERENT
-------------------------------------------

 * Currently, the Google Annotation Chart is distinct from the annotations that
   other Google charts (currently area, bar, column, combo, line, and scatter)
   support. In those charts, the annotations are specified in a separate
   datatable column, and displayed as short bits of text that users can hover
   over to see the full annotation text. In contrast, the Annotation Chart
   displays the full annotations on the right-hand side.
