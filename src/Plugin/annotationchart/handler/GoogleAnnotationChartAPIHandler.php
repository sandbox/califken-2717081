<?php

namespace Drupal\annotationchart\Plugin\annotationchart\handler;

use Drupal\annotationchart\AnnotationChartHandlerInterface;

/**
 * Google Annotation Chart API.
 *
 * @Plugin(
 *   id = "gva",
 *   name = "gva",
 *   label = @Translation("Google Annotation Chart API")
 * )
 */
class GoogleAnnotationChartAPIHandler implements AnnotationChartHandlerInterface {

  public $name = 'gva';

  protected $addedJavascript = FALSE;

  /**
   * Renders a chart using the Google Annotation Chart API.
   */
  public function render($chart_id, $data, $options) {

    // Chart options.
    $chart_options = array(
      'title' => $options['title'],
      'color' => $options['color'],
      'color2' => $options['color2'],
    );

    if (strlen($options['graphfield2']) > 2) {
      $chart_options['graphfield2'] = 1;
    }
    else {
      $chart_options['graphfield2'] = 0;
    }

    // Get Keys for Date, Graph and Event fields.
    $datefield = $options['datefield'];
    $datelabel = $options['fields'][$datefield]['label'];
    $eventfield = $options['eventfield'];
    $eventlabel = $options['fields'][$eventfield]['label'];
    $graphfield1 = $options['graphfield'];
    $graphlabel1 = $options['fields'][$graphfield1]['label'];
    $graphfield2 = $options['graphfield2'];
    $graphlabel2 = $options['fields'][$graphfield2]['label'];

    // Build Data Table.
    $table_data = array();

    // First row is the column titles.
    if ($chart_options['graphfield2'] == 1) {
      $table_data[] = array(
        $datelabel,
        $graphlabel1,
        $eventlabel,
        $graphlabel2,
      );
    }
    else {
      $table_data[] = array(
        $datelabel,
        $graphlabel1,
        $eventlabel,
      );
    }
    $chart_options['labels'] = $table_data;

    // Populate the Data Table.
    foreach ($data as $row) {
      $thisrow = array();
      $thisrow[0] = $row[$datefield];
      $thisrow[1] = (float) $row[$graphfield1]->__toString();
      $thisrow[2] = $row[$eventfield];
      if ($chart_options['graphfield2'] == 1) {
        $thisrow[3] = (float) $row[$graphfield2]->__toString();
      }
      $table_data[] = $thisrow;
    }

    // Build array of the information to pass to the chart.
    $information = array(
      'library' => 'google_visualization',
      'type' => $options['type'],
      'options' => $chart_options,
      'dataArray' => $table_data,
      'chart_id' => $chart_id,
    );

    // Add Drupal.settings for this chart.
    $chart['#attached'] = [
      'drupalSettings' => [
        'annotationchart' => [$chart_id => $information],
      ],
    ];
    return $chart;
  }

  /**
   * Loads the global Javascript required by the Google Annotation Chart API.
   */
  public function postRender() {

    // Don't add the Javascript twice.
    if (!$this->addedJavascript) {
      $js_libs['#attached']['library'][] = 'annotationchart/gva';
      drupal_render($js_libs);
      $this->addedJavascript = TRUE;
    }
  }

  /**
   * Returns whether or not the plugin is available.
   */
  public function available() {

    return TRUE;
  }

  /**
   * Returns an array of supported chart types.
   */
  public function supportedTypes() {

    return array('annotationchart');
  }

}
