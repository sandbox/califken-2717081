<?php

namespace Drupal\annotationchart\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin uses views ui to configure views data for rendering charts.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "annotationchart",
 *   title = @Translation("Annotation Chart"),
 *   module = "annotationchart",
 *   theme = "annotationchart",
 *   help = @Translation("Display the resulting data set as an annotated chart."),
 *   display_types = {"normal"}
 * )
 */
class AnnotationChart extends StylePluginBase {
  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = FALSE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = FALSE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;


  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * This option only makes sense on style plugins without row plugins, like
   * for example table.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['type'] = array('default' => 'annotationchart');

    return $options;
  }

  /**
   * Returns the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $fields = $this->displayHandler->getFieldLabels();

    foreach ($fields as $field => $column) {
      $fieldoptions[$field] = $column;
    }

    $form['datefield'] = array(
      '#type' => 'select',
      '#title' => t('Date Values'),
      '#options' => $fieldoptions,
      '#description' => t('Select the Date field that will be used to chart values and annotations on the timeline.'),
      '#default_value' => empty($this->options['datefield']) ? FALSE : $this->options['datefield'],
      '#empty_value' => FALSE,
    );

    $form['eventfield'] = array(
      '#type' => 'select',
      '#title' => t('Event Values'),
      '#options' => $fieldoptions,
      '#description' => t('Select the Text field that will be used to Annotate the timeline.'),
      '#default_value' => empty($this->options['eventfield']) ? FALSE : $this->options['eventfield'],
      '#empty_value' => FALSE,
    );

    $form['graphfield'] = array(
      '#type' => 'select',
      '#title' => t('Graph Values'),
      '#description' => t("Select the Numeric field that will be used to build the chart's line graph."),
      '#options' => $fieldoptions,
      '#default_value' => empty($this->options['graphfield']) ? FALSE : $this->options['graphfield'],
      '#empty_value' => FALSE,
    );

    $form['color'] = array(
      '#type' => 'color',
      '#title' => t('Color'),
      '#description' => t('Choose the first color on the line graph.'),
      '#default_value' => empty($this->options['color']) ? FALSE : $this->options['color'],
    );

    $form['graphfield2'] = array(
      '#type' => 'select',
      '#title' => t('Graph Values #2'),
      '#description' => t("Select the second Numeric field that will be used to build the chart's line graph."),
      '#options' => $fieldoptions,
      '#default_value' => empty($this->options['graphfield2']) ? FALSE : $this->options['graphfield2'],
      '#empty_value' => FALSE,
    );

    $form['color2'] = array(
      '#type' => 'color',
      '#title' => t('Color #2'),
      '#description' => t('Choose the second color on the line graph.'),
      '#default_value' => empty($this->options['color2']) ? FALSE : $this->options['color2'],
    );

    $form['type'] = array(
      '#type' => 'hidden',
      '#title' => t('Chart type'),
      '#options' => array(
        'annotationchart' => t('annotationchart'),
      ),
      '#default_value' => 'annotationchart',
      '#empty_value' => FALSE,
    );

  }

  /**
   * Gets rendered fields.
   *
   * @see template_preprocess_annotationchart()
   *
   * @return array|null
   *   Rendered fields.
   */
  public function getRenderFields() {
    return $this->rendered_fields;
  }

}
