<?php

namespace Drupal\annotationchart;

/**
 * Provides interfaces used by the module and its plugins.
 */
interface AnnotationChartHandlerInterface {

  /**
   * Renders a chart with a given HTML identifier, data set and options.
   *
   * @param string $chart_id
   *     A unique identifier associated with the chart. The HTML of the chart is
   *   also contained within a div with this identifier.
   * @param array $data
   *     An array of values to be used in the Annotation Chart.
   * @param array $options
   *     An array containing the configuration information that is set in the
   *   View's display style settings configuration form.  This includes the
   *   fields and labels designated as the date, graph and event values in the
   *   chart.
   *
   * @return mixed
   *     A string the identifies the div in which to render the chart.
   */
  public function render($chart_id, $data, $options);

  /**
   * Performs anything that should be considered after rendering.
   */
  public function postRender();

  /**
   * Whether or not the handler is available for rendering.
   */
  public function available();

  /**
   * Returns an array containing the chart types supported by this handler.
   */
  public function supportedTypes();

}
